package ren.main;

import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import java.sql.ResultSet;
import java.sql.Statement;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.SdkClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import java.util.Date;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.lambda.runtime.Context;
import java.sql.SQLException;
import ren.errors.DBException;
import java.sql.DriverManager;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.regions.Regions;
import java.sql.Connection;
import ren.model.Response;
import ren.model.Event;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class Register implements RequestHandler<Event, Response>
{
    private static final Connection con;
    private static final Regions clientRegion;
    private static final AmazonS3 s3Client;
    
    private static Connection connectDB() {
        try {
            String DBURL = System.getenv("PGHOST");
            String DBUSERNAME = System.getenv("PGUSER");
            String DBPASSWORD = System.getenv("PGPASSWORD");
            String DBDATABASE = System.getenv("PGDATABASE");
            String connString = "jdbc:postgresql://"+DBURL+":5432/"+DBDATABASE;
            return DriverManager.getConnection(connString,DBUSERNAME,DBPASSWORD);
        }
        catch (SQLException sqlexcp) {
            throw new DBException("Connection Failure");
        }
    }
    
    public Response handleRequest(final Event event, final Context context) {
        final LambdaLogger logger = context.getLogger();
        final String bucketName = System.getenv("BUCKET_NAME");
        String objectKey = "";
        int id = 0;
        final String query = event.getQuery();
        try {
            logger.log(String.format("Query: %s", query));
            final Statement st = Register.con.createStatement();
            final ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                objectKey = event.nombre+"."+event.tipo;
                id = rs.getInt(1);
            }
        }
        catch (SQLException excp) {
            logger.log("FAILED QUERY: " + query);
            throw new DBException(excp.getMessage());
        }
        try {
            logger.log(String.format("Bucketname: %s; Objectkey: %s", bucketName, objectKey));
            final Date expiration = new Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += 900000L;
            expiration.setTime(expTimeMillis);
            final GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, objectKey).withMethod(HttpMethod.PUT).withExpiration(expiration);
            return new Response(Register.s3Client.generatePresignedUrl(generatePresignedUrlRequest), objectKey, id);
        }
        catch (AmazonServiceException e) {
            e.printStackTrace();
            return null;
        }
        catch (SdkClientException e2) {
            e2.printStackTrace();
            return null;
        }
    }
    
    static {
        con = connectDB();
        clientRegion = Regions.US_EAST_2;
        s3Client = (AmazonS3)((AmazonS3ClientBuilder)AmazonS3ClientBuilder.standard().withRegion(Register.clientRegion)).build();
    }
}
